/*
 * Salix - Go templating engine
 * Copyright (C) 2023 Elara Musayelyan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package salixmd

import (
	"github.com/yuin/goldmark"
	"go.elara.ws/salix"
	"go.elara.ws/salix/ast"
)

func New(md goldmark.Markdown) Tag {
	return Tag{md}
}

type Tag struct {
	md goldmark.Markdown
}

func (t Tag) Run(tc *salix.TagContext, block, args []ast.Node) error {
	data, err := tc.ExecuteToMemory(block, nil)
	if err != nil {
		return err
	}
	return t.md.Convert(data, tc)
}
