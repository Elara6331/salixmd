# salixmd

[Salix](https://gitea.elara.ws/Elara6331/salix) tag for markdown rendering.

Template Example:

```markdown
<div>
    #markdown:
        # Example!
        [example](https://example.com)
    #!markdown
</div>
```

Code Example:

```go
md := goldmark.New(goldmark.WithExtensions(extension.GFM))

tmpl, err := salix.New().ParseFile("example.txt")
if err != nil {
	panic(err)
}

err = tmpl.
	WithTagMap(map[string]salix.Tag{
		"markdown": salixmd.New(md),
	}).
	Execute(os.Stdout)
if err != nil {
	panic(err)
}
```
