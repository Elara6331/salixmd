module go.elara.ws/salixmd

go 1.21.3

require (
	github.com/yuin/goldmark v1.6.0
	go.elara.ws/salix v0.0.0-20231101042701-9a0a20da66da
)
